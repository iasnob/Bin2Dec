<?php

$error = '';
$dec = 0;

if (!empty($_POST['convert']) && !empty($_POST['bin'])) {

	$bin = strrev($_POST['bin']);
	$binary = true;

	for ($i=0; $i<strlen($bin) && $binary; $i++){
		$digit = substr($bin, $i, 1);
		if ($digit !== '0' && $digit !== '1'){
			$error = "Use just zeros and ones in binary.";
			$dec = 0;
			$binary = false;
		}else{
			if ($digit === '1') {
				$dec += 2**$i;
			}
		}
	}
}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Bin2Dec</title>
	</head>
	<body>
		<div>
			<?= $error ?>
		</div>
		<form action="bin2dec.php" method="post">
			<input type="text" name="bin"
			<?php if (!empty($_POST)) { ?>
				value="<?= $_POST['bin'] ?>"
			<?php }else{ ?>
				placeholder="enter bin value"
			<?php } ?>
			>
			<input type="submit" name="convert" value="convert">
			<input type="text" name="dec"	value="<?= $dec ?>">
		</form>
	</body>
</html>
